
import random                           #random import to generate random numbers
import matplotlib.pyplot as plt         #matplot import to create plots

geneLength=70           #gene length has to be 10 times amount of datasetlength
population=10           #population can be any amount, higher numbers will increase run time
mutationRate=8          #mutation can be any amount
datasetLength=7         #datasetlength is the rule which uses 6 variables. extra 1 is added to take the last value


data=open("data2.txt", "r")         #reading dataset txt file from folder
text=data.readlines()               #reading all lines

class Rule:                     #rule class
    def __init__(self, condition, action):
        self.condition=list(condition)
        self.action=action

global_rulebase=[]          #array to insert variables and last value
for i in range(1, 65):
    line = text[i].split(" ")   #splitting each time there is a space
    condition = list(map(int, list(line[0])))       #inserting variable to list
    action = int(line[1][0])                        #taking last value
    global_rulebase.append(Rule(condition, action)) #creating a rule which has variable and last value & inserting them to array


class Individual:                   #individual class
    def __init__(self, genes = None):   #initialize this everytime class is created
        self.genes=genes                #setting genes to none
        self.fitness = 0                #setting fitness to 0
        if genes == None:
            self.genes = []             #array to insert gene
            for i in range(geneLength):     #loop till 70
                if (i+1) % datasetLength == 0:      #using modulus to check if it is 0
                    self.genes.append(random.randint(0, 1)) #if is 0 then insert 0 or 1 (random) to gene array
                else:
                    self.genes.append(random.randint(0, 2)) #if is 0 then insert 0 or 1, 2 (random) to gene array

    def fitness_function(self):     #fitness function for individuals
        self.fitness=0              #initializing fitness to 0
        local_rulebase=[]           #using another rule here
        for i in range(0, geneLength, datasetLength):   #looping it for 10 times
            line=self.genes[slice(i, i+datasetLength, 1)]#inserting 7 values to line array each time
            condition=line[slice(0, datasetLength-1)]    #taking 6 values and intializing them to condition
            action=line[datasetLength-1]                 #taking last value from line array (the one which didnt get taken from condition)
            local_rulebase.append(Rule(condition, action))  #creating a rule and inserting them to array

        for i in range(64):
            for j in range(10):     #looping 10 times only
                matches=0           #intializing matches to 0
                for l in range(datasetLength-1):
                    if local_rulebase[j].condition[l] == global_rulebase[i].condition[l] or local_rulebase[j].condition[l] == 2:
                        matches += 1     #if local rulebase array's variable is equal to global or local rule base is equal to 2  then increment matches
                if matches == datasetLength - 1:    #if matches is equal to 5
                    if local_rulebase[j].action == global_rulebase[i].action: #if local rulebase array's last value is eqal to global
                        self.fitness += 1   #then increment fitness
                    break

class Generation:           #generation class
    def __init__(self, individuals = None): #initialize this everytime class is created
        self.individuals = individuals  #setting individuals to none
        if individuals == None:
            self.individuals = []       #array to insert individuals
            for i in range(population):
                self.individuals.append(Individual()) #making individuals per population

    def fitness_function(self):             #fitness function for generation class
        for individual in self.individuals:
            individual.fitness_function()   #using individual's fitness function

        self.fittest = 0                #intiliazing fittest to 0
        self.lowestFitness = 0          #intiliazing lowestFitness to 0
        total = 0                      #total used to find average fitness
        for i in range(population):    #depending on amount of population it will have better chance for high fitness
            if self.individuals[i].fitness > self.fittest: #if individal fitness is higher then current fitness
                #print("self individal fitness = ",self.individuals[i].fitness, "self fitness = ",self.fittest)
                self.lowestFitness = self.fittest       #initializing lowest fitness
                self.fittest = self.individuals[i].fitness  #initializing individual's best fitness to fittest

            total = total + self.individuals[i].fitness     #adding total amont of fitness

        self.averageFitness=total/population        #dividing by population to find average fitness
        #print("Total = ",total)


def tournamentSelection(generation):        #using tournament selection function
    offspring = []          #initializing empty array

    for i in range(population):         #loop till population amouunt
        candidate1 = random.randint(0, population-1)    #picking random number for  both canditates
        candidate2 = random.randint(0, population-1)
        while candidate1 == candidate2:     #if it is equal to each other then randomize again
            candidate1 = random.randint(0, population-1)
            candidate2 = random.randint(0, population-1)
        if generation.individuals[candidate1].fitness >= generation.individuals[candidate2].fitness:
            offspring.append(generation.individuals[candidate1])    #if candidate 1 fitness is greater than 2 then insert to array
        else:
            offspring.append(generation.individuals[candidate2])#if candidate 2 fitness is greater than 1 then insert to array
    return offspring

def crossover(offspring):   #crossover function
    for i in range(0, population, 2):   #loop till population and increment by 2
        crossoverpopulationoint = random.randint(0, population-1)  #takes random number and changes the tails of the 2 individuals
        for j in range(crossoverpopulationoint, geneLength-1):
            offspring[i].genes[j], offspring[i+1].genes[j] = offspring[i+1].genes[j], offspring[i].genes[j]
    return offspring

def mutation(offspring):        #mutation function
    for i in range(population):     #loops till population
        for j in range(geneLength): #loops till geneLength
            if random.randint(0, 100) <= mutationRate:  #if random number is less or equal to mutation rate
                if (j+1) % datasetLength == 0:  #using modulus to see if it is equal to 0
                    if offspring[i].genes[j]==0:    #if genes value is equal to 0
                        offspring[i].genes[j]=1     #then initialize it to 1
                    else:
                        offspring[i].genes[j]=0    #then initialize it to 0
                else:                               #if is not equal to 0
                    if offspring[i].genes[j]==0:    #if genes value is equal to 0
                        offspring[i].genes[j]=random.randint(1, 2)  #pick random number from 1, 2 and initialize
                    elif offspring[i].genes[j]==1:  #if genes value is equal to 1
                        offspring[i].genes[j]=random.choice([0, 2]) #pick random number from 0, 2 and initialize
                    else:
                        offspring[i].genes[j]=random.randint(0, 1)#pick random number from 0, 1 and initialize

    return offspring

def findBestFitness():              #finds max/best fitness only
    list_of_generations = []        #initializing empty array
    list_of_generations.append(Generation())    #insert generations in array
    list_of_generations[0].fitness_function()   #find each one's fitness function
    max = 0         #initialize values to 0
    avg = 0
    plt.xlabel('Generation')    #for plotting
    plt.ylabel('Fitness')
    plotGen = []
    plotBestFit = []

    for i in range(500):        #epoch/generation
        offspring=tournamentSelection(list_of_generations[0])   #using tournament selection on generation value
        offspring=crossover(offspring)      #then crossover
        offspring=mutation(offspring)       #then mutation
        list_of_generations.append(Generation(offspring))   #insert generations in array
        if list_of_generations[i].fittest > max:    #finding max fitness
            max = list_of_generations[i].fittest    #intialize each fitness which is higher than max
        print("Generation: ", i, " Fittest: ",max)
        plotGen.append(i)       #for plotting
        plotBestFit.append(max)
        list_of_generations[i+1].fitness_function() #using fitness function on next value

    plt.plot(plotGen, plotBestFit,"-b", label="Best fitness") #for plotting
    plt.legend(loc="best")
    plt.savefig('ga2_bestfitness_dataset2.png')  #saving figure in folder


def findAvgBestLowFitness():   #finds average, best, and lowest fitness
    list_of_generations = []        #initializing empty array
    list_of_generations.append(Generation())    #insert generations in array
    list_of_generations[0].fitness_function()   #find each one's fitness function
    max = 0         #initialize values to 0
    avg = 0
    max2 = 0
    plt.xlabel('Generation')    #for plotting
    plt.ylabel('Fitness')
    plotGen = []
    plotBestFit = []
    plotAvgFit = []
    plotLowFit = []

    for i in range(50):
        offspring=tournamentSelection(list_of_generations[0])   #using tournament selection on generation value
        offspring=crossover(offspring)      #then crossover
        offspring=mutation(offspring)       #then mutation
        list_of_generations.append(Generation(offspring))   #insert generations in array
        avg = list_of_generations[i].averageFitness     #initializing avg/best/lowest from each generation
        max = list_of_generations[i].fittest
        low = list_of_generations[i].lowestFitness
        print("Generation: ", i, "\tBest Fitness: ",max, "\tAverage Fitness: ",avg, "\tLowest Fitness: ",low)
        plotGen.append(i)               #for plotting
        plotBestFit.append(max)
        plotAvgFit.append(avg)
        plotLowFit.append(low)
        if list_of_generations[i].fittest > max2:    #finding max fitness
            max2 = list_of_generations[i].fittest    #intialize each fitness which is higher than max
        list_of_generations[i+1].fitness_function()
    print("Max fitness: ",max2)
    plt.plot(plotGen, plotBestFit,"-b", label="Best fitness")   #for plotting
    plt.plot(plotGen, plotAvgFit,"-r", label="Average fitness")
    plt.plot(plotGen, plotLowFit,"-g", label="Lowest fitness")
    plt.legend(loc="best")
    plt.savefig('ga2_BestAvgLowFitness_dataset2.png')    #saving figure in folder

findBestFitness()                   #function to find best fitness, remove comment to run it
#findAvgBestLowFitness()            #function to find best/avg/low fitness, remove comment to run it
